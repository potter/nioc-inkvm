
Note:
This repository is only of interest to people in my research group.

Sudo or root privileges is not necessary, but you do need permission
to run KVM.  If there are problems, either `ls -l /dev/kvm` or `sudo
modprobe kvm-intel` may be useful.

## Cheat Sheet:

1. This you probably have already done:

        mkdir {your JPF project directory}
        cd {your JPF project directory}
        hg clone http://babelfish.arc.nasa.gov/hg/jpf/jpf-core
        hg clone https://{YOUR BITBUCKET ID}@bitbucket.org/cyrille.artho/net-iocache

2. Download this and dinkvm:

        cd {your JPF project directory}
        git clone https://bitbucket.org/potter/nioc-inkvm.git
        git clone https://bitbucket.org/potter/dinkvm.git

3. Get Knoppix iso in some directory above dinkvm:

        cd {your JPF project directory}
        ln -s /home/potter/iso-files/KNOPPIX_V6.7.1DVD-2011-09-14-EN.iso .

        # or search for KNOPPIX_V6.7.1DVD-2011-09-14-EN.iso w/ Google and
        # download (sha1 is d42a8b60fb2551979cfc5e3c44940f941a1b425b)

        cd {your JPF project directory}
        wget http://ftp.inf.fh-bonn-rhein-sieg.de/pub/knoppix/dvd/KNOPPIX_V6.7.1DVD-2011-09-14-EN.iso
        sha1sum KNOPPIX_V6.7.1DVD-2011-09-14-EN.iso

3. Run tests:

        cd {your JPF project directory}
        ./dinkvm/dinkvm -show -671 ./tmpvm /-- ./nioc-inkvm/one-step.sh 742 316

4. Clean up:

        ./dinkvm/dinkvm -ls -rm

