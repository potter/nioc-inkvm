#!/bin/bash

# Assuming that everything is installed relative to the current
# directory like this:
# $ ls
# dinkvm jpf-core nioc-inkvm net-iocache

# Then run like this:
# $ ./dinkvm/dinkvm -show -671 ./tmpvm /-- ./nioc-inkvm/one-step.sh 742 316

# Which means: Run this script inside KNOPPIX_V6.7.1DVD-2011-09-14-EN.iso
# (-671), freshly booted (--), removing tmpvm first if necessary
# (/--).  The current host working directory (i.e. the one that vmdir
# is in) will be mounted inside the VM at /home/knoppix/onhost.
# Automatically start vncviewer (-show).  Use jpf-core revision 742
# and net-iocache revision 316.

# Clean up with:
# $ ./dinkvm/dinkvm -ls -rm

reportfail()
{
    echo "Failed ($*). Exiting." 1>&2
    exit 255
}

[ -d ./onhost ] || reportfail "expecting to be run inside dinkmv"
[ -d ./onhost/jpf-core ] || reportfail "expecting current directory to contain jpf-core"

# all parameters are optional
jpfrev="$1"
iocacherev="$2"

# Generate string to make install path unique to test
# for dependancy on a specific install path.
datestring="$(date +%y%m%d-%H%M%S)"

# more output for debugging
set -x

# (0) set JAVA_HOME, needed to do "ant make" for dmtcp
#     something like: export JAVA_HOME=/usr/lib/jvm/java-6-openjdk

jh="$(readlink -f "$(which javac)")"
export JAVA_HOME="${jh%/bin/*}"

# (1) open another xterm for checking out progress
xterm 1>/dev/null 2>/dev/null </dev/null &

# (1a) put all this in a unique folder name to make sure
#      path dependencies do not get into the project

mkdir proj-$datestring
cd proj-$datestring

date >date.start

# (1b) make a site-properties to match unique folder

mkdir ~/.jpf
cat >~/.jpf/site.properties <<EOFsp
# JPF site configuration

jpf-core = \${user.home}/proj-$datestring/jpf-core

# net-iocache extension
jpf-net-iocache = \${user.home}/proj-$datestring/net-iocache
extensions+=,\${jpf-net-iocache}
EOFsp

# (2) knoppix does not have mercurial and thttpd, so load them
for dfile in mercurial_2.2.2-1_i386.deb \
    mercurial-common_2.2.2-1_all.deb \
    thttpd_2.25b-11_i386.deb
do
    [ -f ~/onhost/nioc-inkvm/$dfile ] || reportfail "~/onhost/nioc-inkvm/$dfile not found"
done
time sudo dpkg -i ~/onhost/nioc-inkvm/*.deb


# (3) clone a copy of the mercurial repositories cached on the host
time hg clone ~/onhost/jpf-core
# hack to generalize finding (maybe) most recent the net-iocache directory
hit="$(ls -td  ~/onhost/*cache*/bin | head -n 1)"
[ "$hit" == "" ] && reportfail "could not find {jpf-}net-iocache dir"
time hg clone "${hit%/bin}"

# (4) compile jpf-core
cd jpf-core
[ "$jpfrev" != "" ] && hg update --clean -r "$jpfrev"
hg id -i -n
ant

# (5) compile net-iocache
cd ../*cache*
[ "$iocacherev" != "" ] && hg update --clean -r "$iocacherev"
hg id -i -n
ant
ant make

# (6) run the tests!!
./bin/testall.sh

cd ..

date >date.end

# (7) output start and end times
head date.*

