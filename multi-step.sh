#!/bin/bash

# This script is an experiment to see to what extent a simple
# multi-step test script for dinkvm is possible.

# Assuming that everything is installed relative to the current
# directory like this:
# $ ls
# dinkvm jpf-core nioc-inkvm net-iocache

# Then run the first step like this:
# ./dinkvm  -show -671 ./tmpvm /-- ./nioc-inkvm/multi-step.sh -step 742 316

# Which means: Run this script inside KNOPPIX_V6.7.1DVD-2011-09-14-EN.iso
# (-671), freshly booted (--), removing tmpvm first if necessary
# (/--).  The current host working directory (i.e. the one that vmdir
# is in) will be mounted inside the VM at /home/knoppix/onhost.
# Automatically start vncviewer (-show).  Use jpf-core revision 742
# and net-iocache revision 316. Do one step as defined by the script.

# Run later steps like this:
# ./dinkvm  -show -671 ./tmpvm ... ./nioc-inkvm/multi-step.sh -step
# Which means: Continue with same VM (...), doing one step.

# Save a snapshot along the way:
# ./dinkvm -save ./tmpvm ./mysnapshot

# Do step, starting from snapshot:
# ./dinkvm  -show ./mysnapshot ./tmpvm ... ./nioc-inkvm/multi-step.sh -step
# Which means: Continue from snapshot doing one step.

# Changing "-step" to "-go" in any of the above continues until completion.

# Clean up with:
# $ ./dinkvm/dinkvm -ls -rm

reportfail()
{
    echo "Failed ($*). Exiting." 1>&2
    exit 255
}

[ -d ./onhost ] || reportfail "expecting to be run inside dinkmv"
[ -d ./onhost/jpf-core ] || reportfail "expecting current directory to contain jpf-core"

case "$1" in
    -go | -step)
	cmd="$1"
	shift
	;;
    *)
	cmd="-go"
	;;
esac

# Automatically keep same params as previous steps, but
# allow to be changed for each step.
if [ "$*" = "" ]
then
    set -- $(< script-params)
else
    echo "$*" >script-params
fi

# all parameters are optional
jpfrev="$1"
iocacherev="$2"

# Generate string to make install path unique to test
# for dependancy on a specific install path.
if [ -f datestring ]
then
    datestring="$(< datestring)"
else
    datestring="$(date +%y%m%d-%H%M%S)"
    echo "$datestring" >datestring
fi

# more output for debugging
set -x

atstep=1
[ -f atstep ] && atstep="$(< atstep)"
[ -f atpwd ] && cd "$(< atpwd)"

while true
do
    case "$atstep" in
	1)
	    # (1) open another xterm for checking out progress
	    xterm 1>/dev/null 2>/dev/null </dev/null &

	    # (1a) put all this in a unique folder name to make sure
	    #      path dependencies do not get into the project

	    mkdir proj-$datestring
	    cd proj-$datestring

	    date >date.start

	    # (1b) make a site-properties to match unique folder

	    mkdir ~/.jpf
	    cat >~/.jpf/site.properties <<EOFsp
# JPF site configuration

jpf-core = \${user.home}/proj-$datestring/jpf-core

# net-iocache extension
jpf-net-iocache = \${user.home}/proj-$datestring/net-iocache
extensions+=,\${jpf-net-iocache}
EOFsp
	    ;;
	2)
	    # (2) knoppix does not have mercurial and thttpd, so load them
	    for dfile in mercurial_2.2.2-1_i386.deb \
		mercurial-common_2.2.2-1_all.deb \
		thttpd_2.25b-11_i386.deb
	    do
		[ -f ~/onhost/nioc-inkvm/$dfile ] || reportfail "~/onhost/nioc-inkvm/$dfile not found"
	    done
	    time sudo dpkg -i ~/onhost/nioc-inkvm/*.deb

	    # (3) clone a copy of the mercurial repositories cached on the host
	    time hg clone ~/onhost/jpf-core
	    # hack to generalize finding (maybe) most recent the net-iocache directory
	    hit="$(ls -td  ~/onhost/*cache*/bin | head -n 1)"
	    [ "$hit" == "" ] && reportfail "could not find {jpf-}net-iocache dir"
	    time hg clone "${hit%/bin}"
	    ;;
	3)
	    # (3b) pull the latest changes from the mercurial repositories cached on the host
	    time hg pull ~/onhost/jpf-core
	    # hack to generalize finding (maybe) most recent the net-iocache directory
	    hit="$(ls -td  ~/onhost/*cache*/bin | head -n 1)"
	    [ "$hit" == "" ] && reportfail "could not find {jpf-}net-iocache dir"
	    time hg pull "${hit%/bin}"
	    #
	    # NOTE: this step is new for the multi-step version.  The idea is to 
	    # save a snapshot of the VM after step 2.  Then by starting the script
	    # from that snapshot, testing will quickly jump to step 3, where only
	    # the latest changes need to be pulled.
	    ;;
	4)
	    # (4) compile jpf-core
	    cd jpf-core
	    [ "$jpfrev" != "" ] && hg update --clean -r "$jpfrev"
	    hg id -i -n
	    ant
	    ;;
	5)
	    # (5) compile net-iocache
	    cd ../*cache*
	    [ "$iocacherev" != "" ] && hg update --clean -r "$iocacherev"
	    hg id -i -n
	    ant

	    # set JAVA_HOME, which is needed to do "ant make" for dmtcp
	    #     something like: export JAVA_HOME=/usr/lib/jvm/java-6-openjdk
	    jh="$(readlink -f "$(which javac)")"
	    export JAVA_HOME="${jh%/bin/*}"
	    ant make
	    ;;
	6)
	    # (6) run the tests!!
	    ./bin/testall.sh

	    cd ..

	    date >date.end
	    ;;
	7)
	    # (7) output start and end times
	    head date.*
	    ;;
	*)
	    break
	    ;;
    esac
    (( atstep++ ))
    atpwd="$(pwd)"
    ( 
	cd /home/knoppix 
	echo "$atstep" >atstep
	echo "$atpwd" >atpwd
        # make state variables a little easier to access on the host
	cp script-params atpwd atstep datestring vmdir
    )

    [ "$cmd" = "-step" ] && break
done

